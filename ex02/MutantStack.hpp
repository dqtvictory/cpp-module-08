#ifndef MUTANTSTACK_HPP
#define MUTANTSTACK_HPP

#include <iostream>
#include <stack>

#define GREEN	"\033[92m"
#define YELLOW	"\033[93m"
#define NOCOLOR	"\033[0m"

template <typename T>
class MutantStack : public std::stack<T>
{
public:
	MutantStack(void) {}
	MutantStack(MutantStack const &ms) { *this = ms; }
	MutantStack &operator=(MutantStack const &ms) { return ((MutantStack &)ms); }
	~MutantStack(void) {}

	typedef typename std::stack<T>::container_type::iterator				iterator;
	typedef typename std::stack<T>::container_type::reverse_iterator		reverse_iterator;
	typedef typename std::stack<T>::container_type::const_iterator			const_iterator;
	typedef typename std::stack<T>::container_type::const_reverse_iterator	const_reverse_iterator;

	iterator				begin(void) { return this->c.begin(); }
	reverse_iterator		rbegin(void) { return this->c.rbegin(); }
	const_iterator			begin(void) const { return this->c.begin(); }
	const_reverse_iterator	rbegin(void) const { return this->c.rbegin(); }

	iterator				end(void) { return this->c.end(); }
	reverse_iterator		rend(void) { return this->c.rend(); }
	const_iterator			end(void) const { return this->c.end(); }
	const_reverse_iterator	rend(void) const { return this->c.rend(); }

};

template <typename T>
std::ostream	&operator<<(std::ostream &os, MutantStack<T> &stack)
{

	bool	sw = false;
	os << "< ";
	for	(typename MutantStack<T>::iterator it = stack.begin();
		it != stack.end(); ++it)
	{
		if (sw)
			os << GREEN;
		else
			os << YELLOW;
		os << *it << ' ';
		sw = !sw;
	}
	os << NOCOLOR << '>';
	return (os);
}

#endif
