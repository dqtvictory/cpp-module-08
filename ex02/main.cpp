#include <iostream>
#include "MutantStack.hpp"

template <typename T>
void	displayStackInfo(MutantStack<T> &stack)
{
	bool	sw = false;

	std::cout << "Current stack: " << stack << '\n';

	std::cout << "Inverted view: < ";
	for (typename MutantStack<T>::reverse_iterator it = stack.rbegin(); it != stack.rend(); it++)
	{
		if (sw)
			std::cout << GREEN;
		else
			std::cout << YELLOW;
		std::cout << *it << ' ';
		sw = !sw;
	}
	std::cout << NOCOLOR << ">\n";

	std::cout << "Size of stack: " << stack.size() << '\n';
	std::cout << "Top item: " << stack.top() << '\n';
}

int	main(void)
{
	std::cout << "\n========== INT STACK ==========\n";
	{
		MutantStack<int>	ms;

		for (int i = 0; i < 10; i++)
		{
			std::cout << "Pushing " << i * 10 << " on to the stack\n";
			ms.push(i * 10);
		}
		displayStackInfo(ms);
		std::cout << "Now popping 3 items from stack\n";
		ms.pop(); ms.pop(); ms.pop();
		displayStackInfo(ms);
	}

	std::cout << "\n========== STRING STACK ==========\n";
	{
		MutantStack<std::string>	*ms = new MutantStack<std::string>();
		std::string const	arr[] =
		{
			"Hello World", "Bonjour", "I'm a pony",
			"French Bulldog", "42 Paris"
		};

		for (int i = 0; i < 5; i++)
		{
			std::cout << "Pushing '" << arr[i] << "' on to the stack\n";
			ms->push(arr[i]);
		}
		displayStackInfo(*ms);
		std::cout << "Now popping 3 items from stack\n";
		ms->pop(); ms->pop(); ms->pop();
		displayStackInfo(*ms);

		delete ms;
	}

	std::cout << "\n========== CONSTRUCTOR TESTS ==========\n";
	{
		MutantStack<char>	ms1;
		MutantStack<char>	ms2 = ms1;
		MutantStack<char>	ms3(ms2);
		MutantStack<char>	*ms4 = new MutantStack<char>(ms1);
		delete ms4;

		std::cout << "Tests passed\n";
	}
}