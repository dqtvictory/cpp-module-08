#include "Span.hpp"

Span::Span(unsigned int N) :	_isSorted(false),
								_size(N)
{}

Span::Span(Span const &sp) :	_data(sp._data),
								_isSorted(sp._isSorted),
								_size(sp._size)
{}

Span &Span::operator=(Span const &sp)
{
	return ((Span &)sp);
}

Span::~Span(void) {}

void	Span::addNumber(int num) throw(SpanSizeExceededException)
{
	if (_data.size() == _size)
		throw SpanSizeExceededException();
	_isSorted = false;
	_data.push_back(num);
}

size_t	Span::shortestSpan(void) throw(SpanCountTooLow)
{
	if (_size < 2 || _data.size() < 2)
		throw SpanCountTooLow();
	if (!_isSorted)
	{
		std::sort(_data.begin(), _data.end());
		_isSorted = true;
	}

	size_t	minDiff = INT_MAXI - INT_MINI;
	size_t	diff;
	int		curr, next;
	for (std::vector<int>::iterator it = _data.begin();;)
	{
		curr = *it;
		next = *++it;
		diff = static_cast<size_t>(std::abs(curr - next));
		if (diff < minDiff)
			minDiff = diff;
		if (diff == 0 || it == _data.end())
			break;
	}
	return (minDiff);
}

size_t	Span::longestSpan(void) throw(SpanCountTooLow)
{
	if (_size < 2 || _data.size() < 2)
		throw SpanCountTooLow();
	return (static_cast<size_t>(
		*std::max_element(_data.begin(), _data.end()) - 
		*std::min_element(_data.begin(), _data.end())
		));
}
