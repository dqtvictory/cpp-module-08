#ifndef SPAN_HPP
#define SPAN_HPP

#include <vector>
#include <algorithm>
#include <stdexcept>

#define INT_MINI	-2147483648
#define INT_MAXI	2147483647

class Span
{

private:
	std::vector<int>	_data;
	bool				_isSorted;
	size_t const		_size;

public:
	class SpanSizeExceededException : public std::exception
	{
	public:
		virtual char const	*what() const throw()
		{
			return ("Span size exceeded. Cannot add more number");
		}
	};

	class SpanCountTooLow : public std::exception
	{
	public:
		virtual char const	*what() const throw()
		{
			return ("Span has too few elements");
		}
	};

	Span(unsigned int N);
	Span(Span const &sp);
	Span &operator=(Span const &sp);
	~Span(void);

	void	addNumber(int num) throw(SpanSizeExceededException);

	template <typename InputIter>
	void	addNumber(InputIter begin, InputIter end) throw(SpanSizeExceededException)
	{
		if (std::distance(begin, end) + _data.size() > _size)
			throw SpanSizeExceededException();
		_data.insert(_data.end(), begin, end);
		_isSorted = false;
	}

	size_t	shortestSpan(void) throw(SpanCountTooLow);
	size_t	longestSpan(void) throw(SpanCountTooLow);

};

#endif
