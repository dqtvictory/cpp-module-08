#include <iostream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <set>
#include <list>
#include "easyfind.hpp"

#define SIZE		10
#define NBR_TESTS	5

#define FOUND		"\033[92mFOUND\033[0m"
#define NOT_FOUND	"\033[91mNOT FOUND\033[0m"

template <typename T>
static void	testContainer(T &container, char const *name)
{
	int	val = rand() % 20;
	std::cout << "Looking for " << val << " inside " << name << ": ";
	try
	{
		easyfind(container, val);
		std::cout << FOUND << '\n';
	}
	catch(ValueNotFoundException &e)
	{
		std::cout << NOT_FOUND << '\n';
	}
}

int	main(void)
{
	srand(time(NULL));
	std::vector<int>	values(SIZE);
	for (int i = 0; i < SIZE; i++)
		values[i] = rand() % 20;
	std::sort(values.begin(), values.end());

	std::cout << "\033[93mTest array: [ ";
	for (int i = 0; i < SIZE; i++)
		std::cout << values[i] << ' ';
	std::cout << "]\n\033[0m";

	std::cout	<< "\n========== STD::VECTOR ==========\n";
	{
		std::vector<int>	vec(values.begin(), values.end());
		for (int i = 0; i < NBR_TESTS; i++)
			testContainer(vec, "vector");
	}

	std::cout	<< "\n========== STD::LIST ==========\n";
	{
		std::list<int>	list(values.begin(), values.end());
		for (int i = 0; i < NBR_TESTS; i++)
			testContainer(list, "list");
	}

	std::cout	<< "\n========== STD::SET ==========\n";
	{
		std::set<int>	set(values.begin(), values.end());
		for (int i = 0; i < NBR_TESTS; i++)
			testContainer(set, "set");
	}
}